from escpos import printer
import sys

p = printer.Serial(
    devfile="/dev/ttyUSB0",
    baudrate=19200,
    bytesize=8,
    timeout=1,
    parity="N",
    stopbits=1,
    xonxoff=False,
    dsrdtr=True,
)
p.image(sys.argv[1])
p.cut()
